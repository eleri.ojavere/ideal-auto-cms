<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::middleware('cache.headers:public;max_age=10;s_maxage=10;etag')->group(function () {
    Route::post('/contact-us', 'MailController@contactUs');
    Route::get('/content', 'ContentController@getContent');
    Route::get('/listings/{Id}', 'ListingsController@getListing')->where('id', '[0-9]+');;
    Route::get('/listings', 'ListingsController@getListings');
    Route::options('{any}', function () {
        return response('');
    });
});
