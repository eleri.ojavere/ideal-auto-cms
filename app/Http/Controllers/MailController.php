<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function contactUs(Request $request)
    {
        $customerData = $request->validate([
            'name' => 'required|max:255',
            'phone' => '',
            'email' => 'required|email',
            'content' => 'required',

        ]);



        Mail::send(new ContactUsMail($customerData));

        return response($customerData, 200);
    }
}
