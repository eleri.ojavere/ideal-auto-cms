<?php

namespace App\Http\Controllers;

use App\Content;
use Illuminate\Http\Request;
use OptimistDigital\NovaPageManager\Models\Page;
use OptimistDigital\NovaSettings\Models\Settings;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use App\Helpers\ContentHelper;
use OptimistDigital\NovaRedirects\Models\Redirect;

class ContentController extends Controller
{
    private ContentHelper $helper;

    public function __construct()
    {
        $this->model = new Content;
        $this->helper = new ContentHelper;
    }

    public function getStructure()
    {
        return [
            'locales' => $this->helper->getLocales(),
            'pages' => $this->getPagesStructure(),


        ];
    }

    public function getActiveLocaleSlug($url, $localeSlugs)
    {
        foreach ($localeSlugs as $slug) {
            if (Str::startsWith($url, '/' . $slug)) {
                return '/' . $slug;
            }
        }
    }

    public function getContent(Request $request)
    {

        $querySlug = $request->query('slug');
        $redirect = Redirect::where('from_url', $querySlug)->first();
        if (isset($redirect)) {
            return $redirect;
        }

        $locales = $this->helper->getLocales();

        $localeSlugs = [];
        if ($locales['locales']) {
            $localeSlugs = array_keys($locales['locales']);
        }

        $routeStructure = $this->helper->getRouteStructure();

        $activeLocaleSlug = $this->getActiveLocaleSlug($querySlug, $localeSlugs);
        $activeLocale = Str::replaceFirst('/', '', $activeLocaleSlug);

        $routeData = $routeStructure[$querySlug] ?? null;

        if (isset($routeData)) $content = $this->getPageContent($routeData);




        if (empty($content)) {
            if ($activeLocale) {
                $this->helper->setLocale($activeLocale);
            } else {
                $this->helper->setLocale('ee');
            }

            return [
                'content' => [
                    'localePaths' => $this->helper->getLocalePaths([]),
                    'data' => null
                ],
                'structure' => $this->getStructure(),
            ];
        }

        return [
            'content' => $content,
            'structure' => $this->getStructure(),
        ];
    }


    private function getPageContent($routeData)
    {
        $page = $this->helper->getPage($routeData['id']);
        $page['localePaths'] = $this->helper->getLocalePaths($routeData['localePaths']);
        $this->helper->setLocale($routeData['locale']);
        return $page;
    }

    protected function getPagesStructure()
    {
        $formatPages = function (Collection $pages) use (&$formatPages) {
            $data = [];
            $pages->each(function ($page) use (&$data, &$formatPages) {
                $localeChildren = Page::where('locale_parent_id', $page->id)->where(function ($query) {
                    $query->where('published', true);
                })->whereDoesntHave('childDraft')->get();

                $_pages = collect([$page, $localeChildren])->flatten();
                $_data = [
                    'id' => $_pages->pluck('id', 'locale'),
                    'name' => $_pages->pluck('name', 'locale'),
                    'path' => $_pages->pluck('path', 'locale'),
                    'title' => $_pages->pluck('data.title', 'locale'),
                    'template' => $page->template,
                ];

                $children = Page::where('parent_id', $page->id)->where(function ($query) {
                    $query->where('published', true);
                })->whereDoesntHave('childDraft')->get();

                if ($children->count() > 0) {
                    $_data['children'] = $formatPages($children);
                }

                $data[] = $_data;
            });
            return $data;
        };
    }
}
