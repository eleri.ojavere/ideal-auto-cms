<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use OptimistDigital\MediaField\Models\Media;

class ListingsController extends Controller
{
    public function getListing(Request $request, $id)
    {
        return Listing::find($id);
        return $request->get('user.profile', array('id' => $id));
    }

    public function getListings()
    {

        $query = Listing::query();

        return

            $query->get();
    }
}
