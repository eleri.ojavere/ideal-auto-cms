<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OptimistDigital\MediaField\Models\Media;

class Listing extends Model
{
    // public function getThumbnailAttribute($thumbnailId)
    // {
    //     $image = Media::whereId($thumbnailId)->first();

    //     return ($image->url);
    // }

    public function toArray()
    {

        $thumbnail = Media::whereId($this->thumbnail)->first();

        $original = parent::toArray();

        return array_merge($original, [
            'thumbnail' => $thumbnail->url

        ]);
    }
}
