<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;
use OptimistDigital\MultiselectField\Multiselect;
use OptimistDigital\MediaField\MediaField;


class Listing extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Listing::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'model'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {

        return [
            ID::make()->sortable(),

            MediaField::make('Car Image', 'thumbnail'),

            Text::make('Make and model', 'model')
                ->rules('required', 'max:100'),
            Text::make('Mileage (km)', 'mileage')->rules('required', 'max:25'),
            Text::make('Body type', 'body_type')->rules('required', 'max:25'),
            Text::make('Drivetrain', 'drivetrain')->rules('required', 'max:25'),
            Text::make('Year', 'year')->rules('required', 'max:25'),
            Multiselect::make('Fuel', 'fuel')
                ->options([
                    'B' => 'Bensiin',
                    'D' => 'Diisel',
                ])
                ->rules('required')
                ->max(1),
            Multiselect::make('Transmission', 'transmission')
                ->options([
                    'A' => 'Automaat',
                    'M' => 'Manuaal',
                    'S-A' => 'Semi-Automaat',

                ])
                ->rules('required')
                ->max(1),
            Text::make('Price (EUR)', 'price')->rules('required', 'max:20')

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
