<?php

namespace App\Nova\Templates;

use Illuminate\Http\Request;
use OptimistDigital\NovaPageManager\Template;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Trix;
use Whitecube\NovaFlexibleContent\Flexible;
use OptimistDigital\MediaField\MediaField;



class HeaderRegion extends Template
{
    public static $type = 'region';
    public static $name = 'HeaderRegion';

    public function fields(Request $request): array
    {
        return [
            Flexible::make('Header content')
            ->fullWidth()
            ->button('Add content')
            ->limit(2)
            ->addLayout('Logo', 'logo', [
                Image::make('Logo') ,
                
            ])

            ->addLayout('Menu', 'menu', [
                Flexible::make('Menu')
                ->fullWidth()
                ->limit(3)
                ->button('Add a menu item')
                ->addLayout('Menu item', 'menu',[
                Text::make('Menu item')
                ]),

            ])

            




        ];

        
    }
}
