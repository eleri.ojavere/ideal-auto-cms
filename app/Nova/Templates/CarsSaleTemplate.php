<?php

namespace App\Nova\Templates;

use Illuminate\Http\Request;
use OptimistDigital\NovaPageManager\Template;
use Illuminate\Support\Facades\Storage;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Textarea;
use Whitecube\NovaFlexibleContent\Flexible;

class CarsSaleTemplate extends Template
{
    public static $type = 'page';
    public static $name = 'cars-sale';
    public static $seo = false;
    public static $view = null;

    public function fields(Request $request): array
    {
        return [
            Flexible::make('Content')
                ->fullWidth()
                ->addLayout('Description section', 'description', [
                    Text::make('Title'),

                    Flexible::make('Promises')
                        ->button('Add a promise')
                        ->addLayout('Promise', 'promises', [
                            Text::make('Promise')
                        ]),




                ])
        ];
    }
}
