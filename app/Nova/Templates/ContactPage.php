<?php

namespace App\Nova\Templates;

use Illuminate\Http\Request;
use OptimistDigital\NovaPageManager\Template;
use Illuminate\Support\Facades\Storage;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Textarea;
use Whitecube\NovaFlexibleContent\Flexible;

class ContactPage extends Template
{
    public static $type = 'page';
    public static $name = 'contact-page';
    public static $seo = false;
    public static $view = null;

    public function fields(Request $request): array
    {
        return [
            Flexible::make('Content')
                ->fullWidth()
                ->addLayout('Google maps section', 'google-maps', [
                    Image::make('Image', 'thumbnail')->disableDownload()
                        ->resolveResponseUsing(function ($fileName) {
                            return env('APP_URL') . Storage::url($fileName);
                        }),

                ])
        ];
    }
}
