<?php

namespace App\Nova\Templates;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use OptimistDigital\NovaPageManager\Template;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Textarea;
use Whitecube\NovaFlexibleContent\Flexible;



class TestTemplate extends Template
{
    public static $type = 'page';
    public static $name = 'home-page';
    public static $seo = false;
    public static $view = null;

    public function fields(Request $request): array

    {
        return [
            Flexible::make('Content')
                ->fullWidth()
                ->addLayout('Services section', 'services', [
                    Text::make('Slogan'),
                    Text::make('Title'),

                    Flexible::make('Services')
                        ->button('Add a service')
                        ->addLayout('Service', 'services', [
                            Text::make('Service')
                        ]),

                    Image::make('Background image', 'thumbnail')
                        ->disableDownload()
                        ->storeOriginalName('original_name')
                        ->resolveResponseUsing(function ($fileName) {
                            return env('APP_URL') . Storage::url($fileName);
                        }),


                ])

                ->addLayout('Exchange section', 'exchange', [
                    Text::make('Title'),
                    Textarea::make('Description'),

                    Flexible::make('Promises')
                        ->button('Add a promise')
                        ->addLayout('Promise', 'promises', [
                            Text::make('Promise')
                        ]),

                    Text::make('Button 1'),
                    Text::make('Button 2'),

                ])
                ->addLayout('Claims section', 'claims', [
                    Text::make('Title'),
                    Flexible::make('Services')
                        ->button('Add a service')
                        ->addLayout('Service', 'services', [
                            Text::make('Service')
                        ]),
                    Text::make('Button 1'),
                    Text::make('Button 2'),
                    Text::make('Additional actions'),
                    Image::make('Background image')
                        ->disableDownload()
                        ->resolveResponseUsing(function ($fileName) {
                            return env('APP_URL') . Storage::url($fileName);
                        }),

                ])

                ->addLayout('Car wash section', 'carwash', [
                    Flexible::make('Honeycomb titles')
                        ->limit(3)
                        ->button('Add a title')
                        ->addLayout('Title', 'titles', [
                            Text::make('Title')
                        ]),
                    Text::make('Title'),
                    Textarea::make('Description'),
                    Text::make('Button 1'),
                    Image::make('Background image', 'thumbnail')
                        ->disableDownload()
                        ->resolveResponseUsing(function ($fileName) {
                            return env('APP_URL') . Storage::url($fileName);
                        }),

                ])

                ->addLayout('Car wash gallery section', 'carwash-gallery', [

                    Image::make('Left side image', 'thumbnail')
                        ->disableDownload()
                        ->resolveResponseUsing(function ($fileName) {
                            return env('APP_URL') . Storage::url($fileName);
                        }),
                    Flexible::make('Right side images')
                        ->limit(4)
                        ->button('Add an image')
                        ->addLayout('Image', 'images', [
                            Image::make('Image', 'thumbnail')
                                ->disableDownload()
                                ->resolveResponseUsing(function ($fileName) {
                                    return env('APP_URL') . Storage::url($fileName);
                                }),
                        ]),

                ])

                ->addLayout('About us section', 'about-us', [
                    Text::make('Title'),
                    Textarea::make('Description'),
                    Text::make('Button 1'),


                ])

                ->addLayout('Google maps section', 'google-maps', [
                    Image::make('Image', 'thumbnail')->disableDownload()
                        ->resolveResponseUsing(function ($fileName) {
                            return env('APP_URL') . Storage::url($fileName);
                        }),

                ])

        ];
    }
}
