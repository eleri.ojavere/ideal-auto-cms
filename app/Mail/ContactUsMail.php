<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $customerData;

    // public properties are accessible from the view

    /**
     * Create a new message instance.
     *
     * @param LayoutMailRawRequest $request
     */
    public function __construct(array $customerData)
    {
        $this->customerData = $customerData;
    }

    /**
     * Build the message.
     *
     * @return ContactUsEmail
     * @throws Throwable
     */



    public function build()
    {

        return $this->text('emails.contact-us', $this->customerData)->to('eleri.ojavere@gmail.com')->subject('subject');
    }
}
