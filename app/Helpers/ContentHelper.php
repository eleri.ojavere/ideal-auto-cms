<?php

namespace App\Helpers;

use OptimistDigital\NovaPageManager\NovaPageManager;

class ContentHelper
{
    protected $locale;

    public function getSortedRegions()
    {
        return array_map(array($this, 'sortRegionsByLocale'), nova_get_regions());
    }



    public function getLocales()
    {
        return [
            'locales' => nova_lang_get_all_locales(),
            'active' => $this->locale,
        ];
    }

    public function setLocale($locale)
    {
        return $this->locale = $locale;
    }

    public function getPage($id, $previewToken = null)
    {
        if (empty($id)) return null;

        $page = nova_get_page($id, $previewToken);

        return $page;
    }

    public function flatten($input, $key)
    {
        $output = [];
        foreach ($input as $object) {
            $children = isset($object[$key]) ? $object[$key] : [];
            $object[$key] = [];
            $output[] = $object;
            $children = $this->flatten($children, $key);
            foreach ($children as $child) {
                $output[] = $child;
            }
        }
        return $output;
    }

    public function getLocalePaths($localePaths)
    {
        foreach (nova_lang_get_all_locales() as $locale => $value) {
            $localePaths[$locale] = isset($localePaths[$locale]) ? $localePaths[$locale] : "/{$locale}";
        }
        return $localePaths;
    }




    public function getRouteStructure()
    {

        $pageStructure = $this->flatten(nova_get_pages_structure(), 'children');
        return $this->formatStructure($pageStructure);
    }

    public function formatStructure($structure)
    {
        $routesData = [];
        foreach ($structure as $page) {
            $routes = [];
            foreach ($page['locales'] as $locale) {
                $routes += [
                    $page['path']->get($locale) => [
                        'locale' => $locale,
                        'id' => $page['id']->get($locale),
                        'name' => $page['name']->get($locale),
                        'slug' => $page['slug']->get($locale),
                        'template' => $page['template'],
                        'path' => $page['path']->get($locale),
                        'localePaths' => $page['path'],
                    ],
                ];
            }
            $routesData += $routes;
        }
        return $routesData;
    }
}
