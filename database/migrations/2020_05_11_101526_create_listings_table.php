<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->id();
            $table->longText('price');
            $table->longText('thumbnail');
            $table->longText('model');
            $table->longText('mileage');
            $table->longText('body_type');
            $table->longText('drivetrain');
            $table->longText('year');
            $table->longText('fuel');
            $table->longText('transmission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
